({
    getParams: function () {
        var query = location.search.substr(1);
        var result = {};
        query.split("&").forEach(function (part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]).replace(/\+/g, " ");;
        });
        return result;
    },
    approveSupplier : function (component, params) {
        var action = component.get("c.approveMBOSupplier");
        action.setParams({vendorId:params.id, approved:params.approved});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.message', data.message);
            }else{
                this.showToast('Unexpected Error', response.getError()[0].message, 'error');
            }

            component.set('v.isLoading', false);
        });
            
        $A.enqueueAction(action);
    },
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type,
            mode: 'dismissible',
        });
        toastEvent.fire();
    },
})
