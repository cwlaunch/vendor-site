({
    doInit : function(component, event, helper) {
        debugger;
        var params = helper.getParams();
        if(params.id != null && params.approved){
            helper.approveSupplier(component, params);
        }else{
            helper.showToast('Unexpected Error', 'You may have reached this page accidentally' , 'error');
            component.set('v.message', 'Supplier doesn\'t exist');
            component.set('v.isLoading', false);
            
        }
    },
    closeModal: function(component, event, helper) {
        component.find("supplier-approval").notifyClose(true ,false);
    }
})
