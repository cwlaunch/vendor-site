({

    getParams: function () {
        var query = location.search.substr(1);
        var result = {};
        query.split("&").forEach(function (part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]).replace(/\+/g, " ");;
        });
        return result;
    },
    getVendors : function(component){
        component.set('v.isLoading', true);
        var action = component.get("c.getAllVendors");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var vendors = response.getReturnValue();
                this.fuseSearch(component, vendors, null);
                // component.set('v.vendors', this.formatData(vendors));
                this.getFilterValues(component);

            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
                component.set('v.isLoading', false);
            }

            component.set('v.isLoading', false);
        });
            
        $A.enqueueAction(action);
    },
    getVendorById : function(component, id){
        var action = component.get("c.getVendorById");
        action.setParam('idStr', id);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var vendors = response.getReturnValue();
                this.fuseSearch(component, vendors, null);
                // component.set('v.vendors', this.formatData(vendors));
                this.getFilterValues(component);
                this.expand(component, vendors[0].Id);

            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
            }

            component.set('v.isSearching', false);
        });
            
        $A.enqueueAction(action);
    },
    approveSupplier : function (component, params) {
        var action = component.get("c.approveMBOSupplier");
        action.setParams({vendorId:params.id, approved:params.approved});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                this.showSupplierApproval(component, data.message);
            }else{
                this.showToast('Unexpected Error', response.getError()[0].message, 'error');
            }
            component.set('v.isLoading', false);
        });
            
        $A.enqueueAction(action);
    },
    getFilterValues : function(component){
        component.set('v.isLoading', true);
        var inFields = component.get('v.filterFields');
        var action = component.get("c.getFilterMetadata");
        action.setParams({objectName: 'ldm_vendor__Vendor__c', inFields});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                this.buildFilter(component, data);
                component.set('v.isSearching', false);
                component.set('v.isLoading', false);
                

            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
                component.set('v.isSearching', false);
                component.set('v.isLoading', false);
            }

            // component.set('v.isSearching', false);
        });
            
        $A.enqueueAction(action);

    },
    search : function(component, event) {
        component.set('v.isSearching', true);
        var searchText = component.get('v.searchText');
        if(searchText.trim() == "" || searchText.length >= 3){
            var action = component.get("c.searchVendor");
            action.setParams({"searchText": searchText});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data = JSON.parse(response.getReturnValue());
                    this.fuseSearch(component, data, searchText)
                    // component.set('v.vendors', data);
                }else{
                    var error = response.getError()[0].message;
                    this.showToast('Error', error, 'error');
                }
                component.set('v.isSearching', false);
            });
                
            $A.enqueueAction(action);
        }
    },
    updateVendor : function(component, vendor){
        var action = component.get("c.updateVendor");
        action.setParams({"vendor": vendor});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                if(response.success){

                }else{
                    var error = response.message;
                    this.showToast('Error', error, 'error');
                }

            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
            }

            component.set('v.isSearching', false);
        });
            
        $A.enqueueAction(action);
    },

    delete: function(component, vendorId){
        component.set('v.isSearching', true);
        var action = component.get("c.deleteVendor");
        action.setParams({"vendorId": vendorId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.removeVendor(component, vendorId);

            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
            }

            component.set('v.isSearching', false);
        });
            
        $A.enqueueAction(action);
    },
    removeVendor: function(component, vendorId){
        var vendors = component.get('v.vendors');
        for (let index = 0; index < vendors.length; index++) {
            const vendor = vendors[index];
            if(vendor.item.Id == vendorId){
                vendors.splice(index, 1);
                break;
            }
        }

        component.set('v.vendors', vendors);
        component.set('v.vendorId',  null);
        this.toggleDeleteModal(component);
    },
    toggleModal: function(component){

        //reset form
        var element = component.find('edit-modal');
        if($A.util.hasClass(element, "slds-hide")){
            component.set('v.showForm', true);
            document.body.style.overflow = 'hidden';
        }else{
            component.set('v.showForm', false);
            document.body.style.overflow = 'scroll';
        }

        // setTimeout($A.getCallback(() => {
            $A.util.toggleClass(element, "slds-hide");
        // }), 150); 
    },
    toggleDeleteModal: function(component){
        var element = component.find('delete-modal');
        $A.util.toggleClass(element, "slds-fade-in-open");

        var backdrop = component.find('delete-modal-bd');
        $A.util.toggleClass(backdrop, "slds-backdrop_open");

    },
    buildFilter: function(component, filter){
        let filterNameMap = component.get('v.filterFieldNames');
        let filterArray = [];

        for (const key in filter) {
            if (filter.hasOwnProperty(key)) {
                const options = filter[key];
                let map = {};
                map['type'] = key;
                map['name'] = filterNameMap[key];
                map['visible'] = true;
               
                let optionMaps = []
                options.forEach(option => {
                    optionMaps.push({value:option, label:option, checked:false});
                });

                map['options'] = optionMaps;
                filterArray.push(map);
            }
        }

        filterArray.sort(function(a, b){
            var nameA = a.name;
            var nameB = b.name;
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            return 0;
        });

        component.set('v.filterArray', filterArray);
        this.instantiateButtonMenu(component);
    },
    showToast : function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":  type
        });
        toastEvent.fire();
    },
    searchWithFilter : function(component){

        component.set('v.isSearching', true);
        var searchText = component.get('v.searchText');
        var worksGloballyFilter = component.get('v.worksGloballyFilter').map(a => a.option);
        var typeFilter = component.get('v.typeFilter').map(a => a.option);
        var regionFilter = component.get('v.regionFilter').map(a => a.option);
        var areaFilter = component.get('v.areaFilter').map(a => a.option);
        var approvedFilter = component.get('v.approvedFilter').map(a => a.option);
        var approvedOnly = component.get('v.approvedOnly');
        var hasFilters = component.get('v.filters');
        component.set('v.hasFilters', hasFilters.length > 0);

        if(hasFilters){
            component.set('v.showFilterResult', true);
        }

        var action = component.get("c.searchVendorFilter");
        action.setParams({"searchText": searchText, "areaFilter": areaFilter, "worksGloballyFilter": worksGloballyFilter, "typeFilter":typeFilter, "regionFilter":regionFilter, 'approvedOnly': approvedOnly });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                this.fuseSearch(component, data, searchText)
                // component.set('v.vendors', data);
            }else{
                var error = response.getError()[0].message;
                this.showToast('Error', error, 'error');
            }
            component.set('v.isSearching', false);
        });
            
        $A.enqueueAction(action);
    },
    formatData : function(data){
        return data.map((doc, idx) => ({
            item: doc,
            score: 1,
            refIndex: idx
        }));
    },
    formatExpertise: function (data) {
        data.forEach(record => {
            if(record['ldm_vendor__Areas_of_Expertise__c'] != null){
                record['ldm_vendor__Areas_of_Expertise__c'] = record['ldm_vendor__Areas_of_Expertise__c'].replace(/\;/g, ", ");
                var expertise = record.ldm_vendor__Areas_of_Expertise__c.split(',')
                expertise.sort(function(a, b){
                    if(a.trim().toLowerCase() < b.trim().toLowerCase()) { return -1; }
                    if(a.trim().toLowerCase() > b.trim().toLowerCase()) { return 1; }
                    return 0;
                });
                record.ldm_vendor__Areas_of_Expertise__c = expertise.join(', ');
            }
        });

        return data;
    },
    fuseSearch: function (component, data, searchText) {
        var data = this.formatExpertise(data);
        if(searchText == null || searchText.trim().length == 0){
            component.set('v.vendors', this.formatData(data));
            return false;
        }

        var options = {
            includeScore: true,
            includeMatches: true,
            threshold: .3,
            findAllMatches: true,
            minMatchCharLength: 3,
            distance:1000,
            keys: component.get('v.fields')
        };

        var fuse = new Fuse(data, options);
        var results = fuse.search(searchText);
        component.set('v.vendors', results);
    },
    shareEmail: function(component, event){
        //var vendorId = event.currentTarget.dataset.id
        let vendorId = event.getSource().get("v.value");
        var selectedVendor = this.getVendorByIdLocal(component, vendorId);

        // var baseUrl = window.location.href;
        var baseUrl = window.location.origin + window.location.pathname;
        var url = baseUrl + '?id=' + selectedVendor.item.Id;
        //var videoName = encodeURIComponent(video.ldm_vid__Title__c);
        var emailCompose = 'https://mail.google.com/mail/u/0/?view=cm&tf=0&su=Check+Out+' + selectedVendor.item.Name + '&body=' + selectedVendor.item.Name + ':++'+ url + '%0a%0aFor+a+list+of+all+suppliers,+check+out+the+Talent+Development+Supplier+Database:+https://talentdevelopment.force.com/s/suppliers.';
        window.open(emailCompose); 
    },
    copyLinkToClipboard : function (component, event) {
        let vendorId = event.getSource().get("v.value");
        var selectedVendor = this.getVendorByIdLocal(component, vendorId);
        var videoUrl = window.location.origin + window.location.pathname + '?id=' + vendorId;
        const el = document.createElement('textarea');
        el.value = videoUrl;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    
        // this.toggleShareLinkClass(component, event);
        this.showToast("", "Link copied to clipboard!");
      },
    showToast : function(title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            type: 'success',
        });
        toastEvent.fire();
    },
    getVendorByIdLocal: function(component, vendorId){
        let selectedVendor;
        var allVendors = component.get('v.vendors');
        for (let index = 0; index < allVendors.length; index++) {
            const vendor = allVendors[index];
            if(vendor.item.Id == vendorId){
                selectedVendor = vendor;
                break;
            }
        }

        return selectedVendor;
    },
    instantiateButtonMenu: function(component){
        /* NOTE: MenuItems are undefined until visible */
        setTimeout($A.getCallback(() => {
            var buttonMenus = component.find('buttonMenu')
            buttonMenus.forEach(menu => {
                menu.set('v.visible', false);
            });
            component.set('v.isSearching', false);
        }), 100); 
    },
    getUserInfo: function(component){
        var action = component.get("c.getUserInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                var user = data.User;
                component.set('v.user', user);
                component.set('v.canDelete', data.hasDeleteAccess);
                component.set('v.sessionId', data.sessionId);
        
            }else{
                //var error = response.getError()[0].message;
                //this.showToast('Error', error, 'error');
            }
        });
            
        $A.enqueueAction(action);
    },
    expand : function(component, recordId) {
        let element = document.getElementById(recordId);

        if(element == null){
            let _this = this;
            setTimeout($A.getCallback(() => {
                _this.expand(component, recordId)
            }), 250); 
        }else{
            $A.util.toggleClass(element, "collapse");
            $A.util.toggleClass(element, "active");
            var active = $A.util.hasClass(element, "active");
            active ? component.set('v.active', recordId) : component.set('v.active', null);
        }
    },
    showExpertiseEdit : function(component) {
        var element = component.find('expertise-modal');
        $A.util.toggleClass(element, "slds-fade-in-open");

        var backdrop = component.find('expertise-modal-bd');
        $A.util.toggleClass(backdrop, "slds-backdrop_open");
        
    },
    lockScrolling : function(component){
        var allBody = component.getSuper().get("v.body");
        allBody.forEach(function (body) {
            $A.util.addClass(body, "slds-scrollable_none");
        });
    },
    unLockScrolling : function(component){
        var allBody = component.getSuper().get("v.body");
        allBody.forEach(function (body) {
            $A.util.removeClass(body, "slds-scrollable_none");
        });
    },
    showSupplierApproval: function(component, message){
        var modalBody;
        $A.createComponent("c:VendorApprovalComponent", {
            message : message
        },
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('supplier-approval').showCustomModal({
                       header: "Supplier Updated",
                       body: modalBody,
                       showCloseButton: true,
                       cssClass: "",
                       closeCallback: function() {}
                   })
               }
           });

    },
    showDeleteRequest: function(component, supplierId){
        var supplier = this.getVendorByIdLocal(component, supplierId);
        if(supplier != null){
            var _this = this;
            var modalBody;
            $A.createComponent("c:DeleteRequestModalComponent", {
                supplier: supplier.item
            },
            function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('submit-delete-request').showCustomModal({
                        header:'Delete Request',
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "",
                        closeCallback: function() {}
                    });
                }
            });
        }
    }

})