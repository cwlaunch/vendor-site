({
    doInit : function(component, event, helper) {
        var params = helper.getParams()

        if(params != null && params.approved != null){
            helper.approveSupplier(component, params);
            helper.getVendors(component);
        }else if(params != null && params.id != null){
            helper.getVendorById(component, params.id);
        }else{
            helper.getVendors(component);
        }

        helper.getUserInfo(component);
        
    },
    searchKeyChange : function(component, event, helper) {
        var searchText = component.get('v.searchText');
        var isEnterKey = event.keyCode === 13;
        if(isEnterKey){
            helper.searchWithFilter(component);
        }else if (searchText.trim() == ''){
            helper.getVendors(component);
        }

        // var delay = 350;
        // var timer = component.get('v.timer');
        // clearTimeout(timer);
        // timer = setTimeout($A.getCallback(() => {
        //     helper.search(component, event);
        // }), delay); 
        // component.set('v.timer', timer);
    },
    toggleFilter : function(component, event, helper){
        var element = component.find('filter-section');
        $A.util.toggleClass(element, "slds-is-collapsed");
    },
    approveFilterChange : function(component, event, helper){
        debugger;
        event.stopPropagation();
        var source = event.getSource();
        var auraId = source.getLocalId();
        var menuName = source.get('v.value');
        var value = event.getParam("value");
        var label = value ? 'Approved Only' : 'All Suppliers';

        // Find all menu items
        var menuItems = component.find('approvedMenuOptions');
        menuItems.forEach(function (menuItem) {

            if (menuItem.get("v.checked")) {
                menuItem.set("v.checked", false);
            }
            // Check the selected menu item
            if (menuItem.get("v.value") === value) {
                menuItem.set("v.checked", true);
            }
        });
        component.set('v.approvedOnly', value);
        var approvedValues = component.get('v.approvedFilter')
        var buttonMenu = component.find('approvedButtonMenu');
        buttonMenu.set('v.label', label);
        var allFilters = component.get('v.typeFilter').concat(component.get('v.worksGloballyFilter'), component.get('v.regionFilter'),  component.get('v.areaFilter'), component.get('v.approvedFilter'));
        if(value && approvedValues.length == 0){
            allFilters.push({menu:'Approved', option: label});
            approvedValues.push({menu:'Approved', option: label});

            // if(approvedValues.length == 0){
            //     approvedValues.push({menu:'Approved', option: label});
            // }
        }else{
            component.set('v.approvedFilter', [])
        }
        component.set('v.filters', allFilters);
        component.set('v.approvedFilter', approvedValues);

    },
    filterChange : function(component, event, helper){
        event.stopPropagation();
        var source = event.getSource();
        var auraId = source.getLocalId();
        var menuName = source.get('v.value');
        var selectedMenuItemValue = event.getParam("value");
        var count = 0;
        var filters = [];

        // Find all menu items
        var menuItems = component.find('menuOptions');
        menuItems.forEach(function (menuItem) {
            if (menuItem.get("v.value") === selectedMenuItemValue) {
                menuItem.set('v.checked', !menuItem.get('v.checked'));
            }
            if( menuItem.get('v.checked') == true && menuItem.get("v.title") === menuName){
                filters.push({menu: menuName, option: menuItem.get('v.value')});
                count++
            }
        });

        var buttonMenus = component.find('buttonMenu')
        buttonMenus.forEach(menu => {
            if(menu.get('v.value') == menuName){
                menu.set('v.label', count + ' option(s) selected');
                menu.set('v.visible', true);                
            }
        });

        switch(menuName) {
            case 'Works Globally':
              component.set('v.worksGloballyFilter', filters);
              break;
            case 'Region':
                component.set('v.regionFilter', filters);
              break;
            case 'Expertise':
                component.set('v.areaFilter', filters);
                break;
            case 'Type':
                component.set('v.typeFilter', filters);
              break;
            case 'Approved':
                component.set('v.approvedFilter', filters);
                break;
            default:
              // code block
        }

        var allFilters = component.get('v.typeFilter').concat(component.get('v.worksGloballyFilter'), component.get('v.regionFilter'),  component.get('v.areaFilter'), component.get('v.approvedFilter'));
        component.set('v.filters', allFilters);

    },

    blur : function(component, event, helper){
        event.stopPropagation();
    },
    selectAll : function(component, event, helper){
        //var menuName = source.get('v.value');
        var menuName = event.target.getAttribute('data-name');
        var count = 0;
        var filters = [];
        var menuItems = component.find('menuOptions');
        menuItems.forEach(function (menuItem) {
            if(menuItem.get("v.title") === menuName && menuItem.get('v.value') != null){
                //filters.push(menuItem.get("v.value"));
                filters.push({menu: menuName, option: menuItem.get('v.value')});
                menuItem.set('v.checked', true);
                count++;
            }
        });

        if(count > 0){
            var buttonMenus = component.find('buttonMenu')
            buttonMenus.forEach(menu => {
                if(menu.get('v.value') == menuName){
                    menu.set('v.label', count + ' option(s) selected');
                }
            });
        }

        // var baseFilter  = filters.map(a => a.option);
        switch(menuName) {
            case 'Works Globally':
              component.set('v.worksGloballyFilter', filters);
              break;
            case 'Region':
                component.set('v.regionFilter', filters);
              break;
            case 'Expertise':
                component.set('v.areaFilter', filters);
                break;
            case 'Type':
                component.set('v.typeFilter', filters);
              break;
            case 'Approved':
                component.set('v.approvedFilter', filters);
                break;
            default:
              // code block
        }


        var allFilters = component.get('v.typeFilter').concat(component.get('v.worksGloballyFilter'), component.get('v.regionFilter'),  component.get('v.areaFilter'), component.get('v.approvedFilter'));
        component.set('v.filters', allFilters);

    },
    addVendor : function(component, event, helper){
        component.set('v.mode', 'edit');
        component.set('v.modalHeader', 'Add Supplier');
        component.set('v.recordId',  null);
        component.set('v.recordId',  '');
        

        setTimeout($A.getCallback(() => {
            helper.toggleModal(component);
        }), 500); 
    },
    handleSuccess : function(component, event, helper){
        var updatedRecord = JSON.parse(JSON.stringify(event.getParams()));
        helper.showToast(updatedRecord.fields.Name.value,'Successfully Saved', 'success');
        helper.toggleModal(component);
        helper.getVendors(component);
    },
    handleError : function(component, event, helper){
        var updatedRecord = JSON.parse(JSON.stringify(event.getParams()));
        helper.showToast(updatedRecord.fields.Name.value,'Error Saving', 'error');
        helper.toggleModal(component);
    },
    closeModal : function(component, event, helper){
        helper.toggleModal(component);
    },
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');

        if (openSections.length === 0) {
            cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },

    expand : function(component, event, helper) {
        // var prevent = component.get('v.eventHack');
        // if(prevent){component.set('v.eventHack', false); return;}

        
        let recordId = event.currentTarget.dataset.recordid;
        helper.expand(component, recordId); 

        // let element = document.getElementById(id);

        // $A.util.toggleClass(element, "collapse");
        // $A.util.toggleClass(element, "active");

        // var active = $A.util.hasClass(element, "active");
        // active ? component.set('v.active', id) : component.set('v.active', null);
        

    },
    editRecord : function(component, event, helper) {
        event.stopPropagation();

        //Tempshow
        component.set('v.showForm', true);

        component.set('v.modalHeader', 'Edit Supplier');
        let recordId = event.getSource().get("v.value");
        // let recordId = event.currentTarget.dataset.id
        component.set('v.recordId', recordId);
        // component.set('v.mode', 'view');

        setTimeout($A.getCallback(() => {
            helper.toggleModal(component);
        }), 500); 
        
    },
    delete : function(component, event, helper) {
        var vendorId = component.get('v.vendorId');
        helper.delete(component, vendorId);
    },

    clear : function(component, event, helper) {
        debugger;
        component.set('v.searchText', '');
        helper.getVendors(component);
    },
    searchWithFilter : function(component, event, helper) {
        helper.searchWithFilter(component);
    },
    clearFilter : function(component, event, helper) {

        var approvedMenuItems = component.find('approvedMenuOptions');
        if(approvedMenuItems != null){
            approvedMenuItems.forEach(function (menuItem) {
                if(menuItem.get('v.label') == 'All Suppliers'){
                    menuItem.set('v.checked', true);
                }else{
                    menuItem.set('v.checked', false);
                }
            });
        }
        
        var approvedButtonMenu = component.find('approvedButtonMenu');
        approvedButtonMenu.set('v.label','All Suppliers');
        component.set('v.approvedOnly', false);

        var menuItems = component.find('menuOptions');
        if(menuItems != null){
            menuItems.forEach(function (menuItem) {
                menuItem.set('v.checked', false);
            });
        }
        
        var buttonMenus = component.find('buttonMenu')
        buttonMenus.forEach(menu => {
            menu.set('v.label','Select One or More');
        });

        component.set('v.worksGloballyFilter', []);
        component.set('v.regionFilter', []);
        component.set('v.areaFilter', []);
        component.set('v.typeFilter', []);
        component.set('v.filters', []);
        component.set('v.approvedFilter', []);
        component.set('v.showFilterResult', false);

        // helper.getVendors(component);
        // var element = component.find('filter-section');
        // $A.util.toggleClass(element, "slds-is-collapsed");
    },
    removeFilter : function(component, event, helper) {
        var label = event.getParam('name');
        var removedFilter;
        
        var allFilters = component.get('v.filters');
        for (let index = 0; index < allFilters.length; index++) {
            const filter = allFilters[index];
            if(filter.option == label){
                removedFilter = filter;
                allFilters.splice(index, 1);
                break;
            }
        }

        component.set('v.filters', allFilters);
        component.set('v.hasFilters', allFilters != 0);

        var worksGloballyFilter = component.get('v.worksGloballyFilter').filter(obj => obj.option != removedFilter.option);
        var typeFilter = component.get('v.typeFilter').filter(obj => obj.option != removedFilter.option);
        var regionFilter = component.get('v.regionFilter').filter(obj => obj.option != removedFilter.option);
        var areaFilter = component.get('v.areaFilter').filter(obj => obj.option != removedFilter.option);
        var approvedFilter = component.get('v.approvedFilter').filter(obj => obj.option != removedFilter.option);

        if(approvedFilter.length == 0){
            component.set('v.approvedOnly', false);
            var buttonMenu = component.find('approvedButtonMenu');
            buttonMenu.set('v.label', 'All Suppliers');

            var menuItems = component.find('approvedMenuOptions');
            if(menuItems != null){
                menuItems.forEach(function (menuItem) {

                    if (menuItem.get("v.label") === 'All Suppliers') {
                        menuItem.set("v.checked", true);
                    }else{
                        menuItem.set("v.checked", false);
                    }
                });
            }
        }


        //TODO: Test
        var menuItems = component.find('menuOptions');
        if(menuItems != null){
            menuItems.forEach(function (menuItem) {
                if(menuItem.get('v.label') == label){
                    menuItem.set('v.checked', false);
                }
                
            });
        }

        var buttonMenus = component.find('buttonMenu')
        buttonMenus.forEach(menu => {
            var count = 0;
            if(menu.get('v.value').toLowerCase() == 'EXPERTISE'.toLowerCase()){
                count = areaFilter.length;
            }else if(menu.get('v.value').toLowerCase() == 'REGION'.toLowerCase()){
                count = regionFilter.length;
            }else if(menu.get('v.value').toLowerCase() == 'Type'.toLowerCase()){
                count = typeFilter.length;
            }
            var buttonText = count == 0 ? 'Select One or More' : count + ' option(s) selected'
            menu.set('v.label', buttonText);
        });


        component.set('v.worksGloballyFilter', worksGloballyFilter);
        component.set('v.typeFilter', typeFilter);
        component.set('v.regionFilter', regionFilter);
        component.set('v.areaFilter', areaFilter);
        component.set('v.approvedFilter', approvedFilter);
        helper.searchWithFilter(component);
    },
    editFilter : function(component, event, helper) {
        component.set('v.showFilterResult', false);
    },
    shareEmail: function(component, event, helper){
        event.stopPropagation();
        helper.shareEmail(component, event);
    },
    copyLinkToClipboard : function(component, event, helper){
        event.stopPropagation();
        helper.copyLinkToClipboard(component, event);
    },
    openDeleteModal : function(component, event, helper){
        var id = event.getSource().get("v.value");
        var canDelete = component.get('v.canDelete');
        if(canDelete){
            component.set('v.vendorId', id);
            helper.toggleDeleteModal(component);
        }else{
            component.set('v.vendorId', id);
            helper.showDeleteRequest(component, id);
        }
    },

    closeDeleteModal : function(component, event, helper){
        component.set('v.vendorId', null);
        helper.toggleDeleteModal(component);
    },

    toggleDeleteModal : function(component, event, helper){
        var vendorId = event.getParam("value");
        component.set('v.vendorId', vendorId);
        helper.toggleDeleteModal(component);
    },

    onload: function(component, event, helper){        
    },

    onsubmit: function(component, event, helper){       
        debugger; 
        const fields = event.getParam('fields');
        if(fields.ldm_vendor__Areas_of_Expertise__c != null){
            var expertise = fields.ldm_vendor__Areas_of_Expertise__c.split(';');
            expertise.sort(function(a, b){
                if(a.trim().toLowerCase() < b.trim().toLowerCase()) { return -1; }
                if(a.trim().toLowerCase() > b.trim().toLowerCase()) { return 1; }
                return 0;
            });
            fields.ldm_vendor__Areas_of_Expertise__c = expertise.join(';');
        }
    },
    addExpertise : function(component, event, helper){
        var sessionId = component.get('v.sessionId');
        var modalBody;
        var _helper = helper;
        $A.createComponent("c:AddExpertiseModalComponent", {
            sessionId: sessionId,
            valueUpdated : component.getReference('v.valueUpdated')
        },
            function (content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('add-expertise-modal').showCustomModal({
                        header: "Add Expertise",
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "slds-modal_small",
                        closeCallback: function (data) {
                            var updated = component.get('v.valueUpdated');
                            if(updated){
                                location.reload();
                            }
                        }
                    })
                }
            });
    },
    getFilterValues : function(component, event, helper){
        helper.getFilterValues(component);
    }
})