({
    submitRequest : function(component, event, helper) {
        var reason = component.get('v.reason');
        if(reason != null && reason.trim() != ''){
            helper.submitRequest(component);
        }else{
            helper.showToast('Required', 'Reason field is required', 'warning');
        }
    },
    closeModal: function(component, event, helper) {
        helper.closeModal(component);
    }
})
