({
    submitRequest : function(component) {
        component.set('v.isLoading', true);
        var supplier = component.get('v.supplier');
        var reason = component.get('v.reason');
        var baseUrl = window.location.origin + window.location.pathname;
        var supplierLink = baseUrl + '?id=' + supplier.Id;
        var action = component.get("c.submitDeleteRequest");
        action.setParams({supplierId:supplier.Id, reason:reason, supplierLink:supplierLink});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                this.showToast('', data.message, data.toast);
                this.closeModal(component);
            }else{
                this.showToast('Unexpected Error', response.getError()[0].message, 'error');
            }

            component.set('v.isLoading', false);
        });
            
        $A.enqueueAction(action);
    },
    showToast : function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":  type
        });
        toastEvent.fire();
    },
    closeModal : function(component){
        component.find('submit-delete-request').notifyClose();
    }
})
