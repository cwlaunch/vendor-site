({
    submitPicklistValues : function(component) {
        component.set('v.isLoading', true);
        var sessionId =component.get('v.sessionId');
        var newValueArray = component.get('v.newValueArray');
        var existingExpertiseValues = component.get('v.existingExpertiseValues');
        var allValues = newValueArray.concat(existingExpertiseValues);
        
        var _this = this;
        var data = this.buildMetaData(component, allValues);
        var Conn = new jsforce.Connection({ accessToken: sessionId });
        Conn.metadata.upsert('CustomField', data, function(err, results) {
            debugger;
            if (err) { 
                _this.showToast('Error', err, 'error');
            }else{
                component.set('v.valueUpdated', true);
                _this.showToast('Success', 'Expertise successfully updated', 'success');
                _this.closeModal(component);
            }
            component.set('v.isLoading', false);
        });
    },
    buildMetaData: function(component, allValues){
        var metadata = [{
			fullName: 'ldm_vendor__Vendor__c.ldm_vendor__Areas_of_Expertise__c',
			type: 'MultiselectPicklist',
			label: 'Expertise',
			visibleLines: 4,
			valueSet: {
				valueSetDefinition: {
                    sorted: 'true',
					value: allValues
				}
			}
      }];
      return metadata;
    },
    getCurrentPicklistValues : function(component){
        var sessionId = component.get('v.sessionId');
        var Conn = new jsforce.Connection({ accessToken: sessionId });
        Conn.metadata.read('CustomObject', 'ldm_vendor__Vendor__c', function(err, metadata) {
            if (err) { console.error(err); }
            for (var i=0; i < metadata.fields.length; i++) {
                if(metadata.fields[i].fullName == 'ldm_vendor__Areas_of_Expertise__c'){
                    component.set('v.existingExpertiseValues', metadata.fields[i].valueSet.valueSetDefinition.value);
                    break;
                }
            }
        });
        component.set('v.isLoading', false);
    },
    closeModal: function(component) {
        component.find("add-expertise-modal").notifyClose();
    },
    showToast : function(title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type":  type
        });
        toastEvent.fire();
    },
})