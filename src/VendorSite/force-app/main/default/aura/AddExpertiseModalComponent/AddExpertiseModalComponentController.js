({
    doInit : function(component, event, helper) {
        debugger;
        helper.getCurrentPicklistValues(component);
    },
    closeModal: function(component, event, helper) {
        helper.closeModal(component);
    },
    add : function(component, event, helper) {

        var moveForwards = false;
        if(event.keyCode == null){
            moveForwards = true;
        }

        if(event.keyCode == 13){
            moveForwards = true;
        }

        if(!moveForwards){
            return;
        }

        var existingExpertiseValues = component.get('v.existingExpertiseValues');
        var newValue = component.get('v.newValue');
        var newValueArray = component.get('v.newValueArray');
        
        var found = newValueArray.filter(val => val.fullName.trim().toLowerCase() == newValue.trim().toLowerCase());
        var exist = existingExpertiseValues.filter(val => val.fullName.trim().toLowerCase() == newValue.trim().toLowerCase());
        if(found.length == 0 && exist.length == 0){
            newValueArray.push({fullName: newValue, label:newValue, default:false});
            component.set('v.newValue', '');
        }else{
            helper.showToast('', newValue + ' already exist', 'warning');
        }
        
        component.set('v.newValueArray', newValueArray);
    },
    remove : function(component, event, helper) {
        var value = event.getSource().get('v.label');
        var newValueArray = component.get('v.newValueArray');
        for (let index = 0; index < newValueArray.length; index++) {
            if(newValueArray[index].fullName.trim().toLowerCase() == value.trim().toLowerCase()){
                newValueArray.splice(index,1);
            }
        }
        component.set('v.newValueArray', newValueArray);
    },
    submitPicklistValues : function(component, event, helper) {
        helper.submitPicklistValues(component);
    },
})