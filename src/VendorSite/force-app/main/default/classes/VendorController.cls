public class VendorController {
    
    public class UpsertResponse{
        public Boolean success = true;
        public String message = '';
        public Vendor__c vendor = new Vendor__c();
    }

    public class UserData{
        public User User;
        public String sessionId;
        public Boolean hasDeleteAccess;
    }

    /**
     * @name getAllVendors
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static List<Vendor__c> getAllVendors(){
        return [SELECT Id, Name, Notes__c, Salesforce_Point_s_of_Contact__c, Region__c, Website__c, Phone__c, City__c, State__c, Country__c, Works_Globally__c, Approved_MBO_Contractor__c, Approved_Vendor_in_Coupa__c, Ballpark_Day_Rate__c, Street__c, Type__c, Point_of_Contact_Email__c, Point_of_Contact_Name__c, Courses_Offered__c, Areas_of_Expertise__c, Additional_Expertise__c, Existing_Salesforce_Experience__c, CreatedBy.Name, CreatedBy.Email, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.Email, CreatedDate from Vendor__c ORDER BY Name ASC];
    }


    /**
     * @name getAllVendors
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static List<Vendor__c> getVendorById(String idStr){
        return [SELECT Id, Name,  Salesforce_Point_s_of_Contact__c, Notes__c, Region__c, Website__c, Phone__c, City__c, State__c, Country__c, Works_Globally__c, Approved_MBO_Contractor__c, Approved_Vendor_in_Coupa__c, Ballpark_Day_Rate__c, Street__c, Type__c, Point_of_Contact_Email__c, Point_of_Contact_Name__c, Courses_Offered__c, Areas_of_Expertise__c, Additional_Expertise__c, Existing_Salesforce_Experience__c, CreatedBy.Name, CreatedBy.Email, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.Email, CreatedDate from Vendor__c WHERE Id =: idStr ORDER BY Name ASC];
    }


    /**
     * @name deleteVendor
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static String deleteVendor(String vendorId){
        delete [SELECT Id from Vendor__c WHERE Id =: vendorId];
        return vendorId;
    }

    /**
     * @name searchVendor
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static String searchVendor(String searchText){
    List<Vendor__c> searchResult = new List<Vendor__c>();
        if(searchText.trim() != ''){
            searchText = '*' + searchText + '*';
            searchResult  =  [FIND :searchText IN ALL FIELDS RETURNING Vendor__c(Id, Name,  Salesforce_Point_s_of_Contact__c, Notes__c, Region__c, Website__c, Phone__c, City__c, State__c, Country__c, Works_Globally__c, Approved_MBO_Contractor__c, Approved_Vendor_in_Coupa__c, Ballpark_Day_Rate__c, Street__c, Type__c, Point_of_Contact_Email__c, Point_of_Contact_Name__c, Courses_Offered__c, Areas_of_Expertise__c, Additional_Expertise__c, CreatedBy.Name, CreatedBy.Email, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.Email, CreatedDate) WITH SPELL_CORRECTION = true][0];
        }else{
            searchResult = [SELECT Id, Name,  Salesforce_Point_s_of_Contact__c, Notes__c, Region__c, Website__c, Phone__c, City__c, State__c, Country__c, Works_Globally__c, Approved_MBO_Contractor__c, Approved_Vendor_in_Coupa__c, Ballpark_Day_Rate__c, Street__c, Type__c, Point_of_Contact_Email__c, Point_of_Contact_Name__c, Courses_Offered__c, Areas_of_Expertise__c, Additional_Expertise__c, CreatedBy.Name, CreatedBy.Email, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.Email, CreatedDate FROM Vendor__c];
        }
            
        return JSON.serialize(searchResult);
    }

    /**
     * @name searchVendorFilter
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static String searchVendorFilter(String searchText, List<String> worksGloballyFilter, List<String> typeFilter, List<String> regionFilter, List<String> areaFilter, Boolean approvedOnly){
        List<SObject> searchResult = new List<SObject>();

        String areaFilterStr = '';
        for (String filter : areaFilter) {
            areaFilterStr +=   '\'' + filter + '\'' + ',';
        }
        areaFilterStr = areaFilterStr.removeEnd(',');
        
        String baseQuery = 'SELECT Id, Name,  Salesforce_Point_s_of_Contact__c,  Notes__c, Region__c, Website__c, Existing_Salesforce_Experience__c, Phone__c, City__c, State__c, Country__c, Works_Globally__c, Approved_MBO_Contractor__c, Approved_Vendor_in_Coupa__c, Ballpark_Day_Rate__c, Street__c, Type__c, Point_of_Contact_Email__c, Point_of_Contact_Name__c, Courses_Offered__c, Areas_of_Expertise__c, Additional_Expertise__c, CreatedBy.Name, CreatedBy.Email, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.Email, CreatedDate FROM Vendor__c';

        if(typeFilter.size() > 0){
            baseQuery += ' WHERE Type__c IN: typeFilter';
        }

        if(regionFilter.size() > 0){
            baseQuery += baseQuery.contains('WHERE') ? ' AND Region__c IN: regionFilter' : ' WHERE Region__c IN: regionFilter';
        }

        if(areaFilter.size() > 0){
            baseQuery += baseQuery.contains('WHERE') ? ' AND Areas_of_Expertise__c includes (' +  areaFilterStr + ')' : ' WHERE Areas_of_Expertise__c includes (' +  areaFilterStr + ')';
        }

        if(worksGloballyFilter.size() > 0){
            baseQuery += baseQuery.contains('WHERE') ? ' AND Works_Globally__c IN: worksGloballyFilter' :  ' WHERE Works_Globally__c IN: worksGloballyFilter';
        }

        if(approvedOnly){
            baseQuery += baseQuery.contains('WHERE') ? ' AND (Approved_MBO_Contractor__c = true OR Approved_Vendor_in_Coupa__c = true)' : ' WHERE (Approved_MBO_Contractor__c = true OR Approved_Vendor_in_Coupa__c = true)';
        }

        System.debug('baseQuery: ' + baseQuery);
        searchResult = Database.query(baseQuery);
            
        return JSON.serialize(searchResult);
    }

    /**
     * @name getFilterMetadata
     * @description get all picklist values for filtering 
     * @param String objectName
     * @param List<String> inFields
     * @return String
    **/
    @AuraEnabled
    public static String getFilterMetadata(String objectName, List<String> inFields){

        Map<String,List<String>> picklistMap = new Map<String,List<String>>();
       
        for (String fieldName : inFields) {
            List<String> picklistValues = new List<String>();
            SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
            List<Schema.PicklistEntry> pick_list_values = objectType.getDescribe()
                                                        .fields.getMap()
                                                        .get(fieldName)
                                                        .getDescribe().getPickListValues();

            for (Schema.PicklistEntry aPickListValue : pick_list_values) {                   
                picklistValues.add(aPickListValue.getValue()); 
            }

            picklistMap.put(fieldName,picklistValues);

        }
    
        return JSON.serialize(picklistMap);

    }

    /**
     * @name updateVendor
     * @description updates current Vendor 
     * @param Vendor__c vendor
     * @return UpsertResponse
    **/
    @AuraEnabled
    public static UpsertResponse updateVendor(Vendor__c vendor){
        UpsertResponse response = new UpsertResponse();

        try {
            upsert vendor;
            response.vendor = vendor;
            
        } catch (Exception e) {
            response.message = e.getMessage();
            response.success = false;
        }
        
        return response;
    }

    /**
     * @name getUserInfo
     * @description updates current Vendor 
     * @return User
    **/
    @AuraEnabled
    public static String getUserInfo(){
        UserData userData = new UserData();
        userData.sessionId = UserInfo.getSessionId();
        userData.User = [SELECT user.id, user.Email,user.FirstName,user.LastName,user.profile.name,user.Username,user.IsActive FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1][0];
        List<PermissionSetAssignment> hassAccess = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Supplier_Delete_Access' AND AssigneeId =:UserInfo.getUserId()];
        userData.hasDeleteAccess = hassAccess.size() > 0;
        return JSON.serialize(userData);
    }

    /**
     * @name sendDeleteRequest
     * @description updates current Vendor 
     * @return User
    **/
    @AuraEnabled
    public static String submitDeleteRequest(String supplierId, String reason, String supplierLink){
        Map<String,Object> results = new Map<String,Object>();
        List<Vendor__c> suppliers = VendorController.getVendorById(supplierId);

        try{
            if(suppliers.size() > 0){
                Vendor__c supplier = suppliers.get(0);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSubject('Supplier Delete Request');
                mail.setToAddresses(new String[]{'chrisw@launchdm.com', 'rbecker@salesforce.com', 'bbeaudrie@salesforce.com'});
                String message = 'Hi Talent Experience Admin,<br/><br/>The following user has submitted a delete request.<br/><br/>  Name:' + UserInfo.getName() + '<br/>Email: ' + UserInfo.getUserEmail() + '<br/><br/> Supplier Name:  <a href="' +  supplierLink + '">' + supplier.Name + '</a> <br/> Reason: ' + reason;
                mail.setHtmlBody(message);  
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

                results.put('success', true);
                results.put('toast', 'success');
                results.put('message', 'A request to delete this supplier has been sent to the site administrator.'); 
    
            }else{
                results.put('success', false);
                results.put('toast', 'error');
                results.put('message', 'Supplier not found'); 
            }
            
        }catch(Exception e){
            results.put('success', false);
            results.put('toast', 'error');
            results.put('message', e.getMessage());
        }
        
        return JSON.serialize(results);
    }



    // /**
    //  * @name sendMBOEmail
    //  * @description updates current Vendor 
    //  * @return User
    // **/
    // @AuraEnabled
    // public static String sendMBOEmail(Vendor__c vendor){

    //     List<String> contactIds = VendorController.getContactIds();
    //     if(contactIds.size() >  0){
    //         // Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
    //         // mail.setTargetObjectIds(contactIds);
    //         // mail.setWhatIds(new String[]{vendor.Id});
    //         // mail.setTemplateId('00X90000000QHUD');
    //         // Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });

    //         String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    //         String restUrl = baseUrl += 'services/apexrest/occommunities/v1/redeemrewards/contactId=00360000012wvfWAAQ

    //         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    //         mail.setSubject('Is this contractor MBO approved?');
    //         mail.setToAddresses(new String[]{'chrisw@launchdm.com'});
    //         String message = 'Salesforce Talent Experience team would like to know if ' + vendor.Name + '. is an approved MBO provider.';
    //         String body = '<div style="margin-top:10px; margon-bottom:10px">' + message + '</div>';
    //         String yesBtn = '<a href="' + baseUrl + '">Yes</a>';
    //         String noBtn = '<a href="">No</a>';
    //         body += '<div> </div>'
    //         mail.setHtmlBody(body);
            
    //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    //     }
        

    //     return '';
    // }

    // /**
    //  * @name sendMBOEmail
    //  * @description updates current Vendor 
    //  * @return User
    // **/
    // @AuraEnabled
    // public static List<String> getContactIds(){

    //     Set<String> emails = new Set<String>();
    //     Map<String,String> emailName = new Map<String,String>();
    //     for (ldm_vendor__Email_Recipients__c email : ldm_vendor__Email_Recipients__c.getall().values()) {
    //         emails.add(email.ldm_vendor__Email__c);
    //         emailName.put(email.ldm_vendor__Email__c, email.Name);
    //     }

    //     List<String> currentContacts = new List<String>();
    //     for (Contact contact : [SELECT Id, Name, Email from Contact WHERE Email IN: emails]) {
    //         currentContacts.add(contact.Email);
    //     }

    //     List<Contact> contacts = new List<Contact>();
    //     for (String email : emails) {
    //         if(!currentContacts.contains(email)){
    //             Contact emailContact = new Contact();
    //             emailContact.LastName = emailName.get(email);
    //             emailContact.Email = email;
    //             contacts.add(emailContact);
    //         }
    //     }

    //     insert contacts;

    //     Map<String, Contact> alContacts = new Map<String, Contact>([SELECT Id, Name, Email from Contact WHERE Email IN: emails]);
    //     List<String> contactIds = new List<String>();

    //     for (String contactId : alContacts.keySet()) {
    //         contactIds.add(contactId);
    //     }


    //     return contactIds;
    // }

    /**
     * @name approveMBOSupplier
     * @description keyword search pages, files, and video 
     * @param String search --> keyword
     * @return String
    **/
    @AuraEnabled
    public static Map<String,String> approveMBOSupplier(String vendorId, Boolean approved){
        Map<String,String> results = new  Map<String,String>();

        List<Vendor__c> vendors = [SELECT Id, Name, Approved_MBO_Contractor__c from Vendor__c WHERE Id =: vendorId];
        if(vendors.size() > 0){
            Vendor__c updatedVendor = vendors.get(0);
            updatedVendor.Approved_MBO_Contractor__c = approved;
            update updatedVendor;
            String message = approved == true ? ' has been listed as MBO approved' : ' has been listed as not MBO approved';
            results.put('success', 'true');
            results.put('message', 'Thank You! ' + updatedVendor.Name + message);
        }else{
            results.put('success', 'false');
            results.put('message', 'Supplier not found.  This suppliier may have been deleted.');
        }

        return results;
    }
}