@isTest
public with sharing class VendorControllerTest {
    
    @testSetup
    public static void makeData(){
        Vendor__c vendor = new Vendor__c();
        vendor.Name = 'Test';
        vendor.Region__c = 'AMER';
        vendor.Website__c = 'test';
        vendor.Phone__c = '555-555-5555';
        vendor.City__c = 'test';
        vendor.State__c = 'test';
        vendor.Country__c = 'test';
        vendor.Works_Globally__c = 'Yes';
        vendor.Approved_MBO_Contractor__c = true;
        vendor.Approved_Vendor_in_Coupa__c = true;
        vendor.Ballpark_Day_Rate__c = 2.00;
        vendor.Street__c = 'test';
        vendor.Type__c = 'Consulting Firm';
        vendor.Point_of_Contact_Email__c ='test@email.com';
        vendor.Point_of_Contact_Name__c = 'Test';
        vendor.Courses_Offered__c = 'test';
        insert vendor;
    }

    @isTest
    public static void getAllVendors(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        Test.stopTest();
        System.assert(vendors.size() > 0);
    }

    @isTest
    public static void getVendorById(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        List<Vendor__c> foundVendor = VendorController.getVendorById(vendors[0].Id);
        Test.stopTest();
        System.assert(foundVendor.size() > 0);
    }

    @isTest
    public static void deleteVendor(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        String vendorId = VendorController.deleteVendor(vendors[0].Id);
        Test.stopTest();
        System.assert(vendorId == vendors[0].Id);
    }

    @isTest
    public static void searchVendor(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        Id[] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = vendors[0].Id;
        Test.setFixedSearchResults(fixedSearchResults);
        String res = VendorController.searchVendor('test'); 
        Test.stopTest();
        System.assert(res != null);
    }


    @isTest
    public static void searchVendorFilter(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        Id[] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = vendors[0].Id;
        Test.setFixedSearchResults(fixedSearchResults);
        String res = VendorController.searchVendorFilter('test', new String[]{'Yes'},  new String[]{'Consulting Firm'},  new String[]{'AMER'}, new String[]{}, true);
        Test.stopTest();
        System.assert(res != null);
    }

    @isTest
    public static void getFilterMetadata(){
        Test.startTest();
        String res = VendorController.getFilterMetadata('ldm_vendor__Vendor__c', new String[]{'ldm_vendor__Region__c'});
        Test.stopTest();
        System.assert(res != null);
    }

    @isTest
    public static void updateVendor(){
        Test.startTest();
        List<Vendor__c> vendors = VendorController.getAllVendors();
        VendorController.UpsertResponse res = VendorController.updateVendor(vendors[0]);
        Test.stopTest();
        System.assert(res.success == true);
    }

    @isTest
    public static void getUser(){
        Test.startTest();
        String userData = VendorController.getUserInfo();
        Test.stopTest();
        System.assert(userData != null);
    }

    @isTest
    public static void approveMBOSupplier(){
        Test.startTest();
        Vendor__c vendor = [SELECT Id, Name from Vendor__c LIMIT 1];
        Map<String,String> results = VendorController.approveMBOSupplier(vendor.Id, true);
        Test.stopTest();
        System.assert(results.get('success') == 'true');
    }


    @isTest
    public static void submitDeleteRequest(){
        Test.startTest();
        Vendor__c vendor = [SELECT Id, Name from Vendor__c LIMIT 1];
        String res = VendorController.submitDeleteRequest(vendor.Id, 'Test Reason', 'Test Link');
        Map<String,Object> results = (Map<String,Object>)JSON.deserializeUntyped(res);
        Test.stopTest();
        System.assert(results.get('success') == true);
    }
    

}