trigger VendorChangeTrigger on Vendor__c (before insert, before update) {


    for(Vendor__c vendor: Trigger.new){
     
        vendor.Areas_of_Expertise__c = vendor.Areas_of_Expertise__c == null ? '' : vendor.Areas_of_Expertise__c;
        if(vendor.Additional_Expertise__c != null && vendor.Additional_Expertise__c != ''){

            List<String> additionalExpertise = vendor.Additional_Expertise__c.split(',');
            for (String value : additionalExpertise) {

                if(!vendor.Areas_of_Expertise__c.contains(value.trim())){
                    vendor.Areas_of_Expertise__c += ';' + value;
                }
            }
        }
        vendor.Additional_Expertise__c = '';
    }
}